//
//  DatabaseManager.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 17/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import Foundation
import CoreData


class DatabaseManager {
    static let sharedInstance = DatabaseManager()

    //MARK: - Shift methods
    func createShift() -> Shift {
        let shift = NSEntityDescription.insertNewObject(forEntityName: "Shift", into: self.persistentContainer.viewContext) as! Shift
        shift.date = NSDate()
        return shift
    }
    
    //MARK: - Location methods
    func createLocationWith(name: String, numberOfFloors: Int, numberOfAdditionalPlaces: Int, andEmails: String) {

        let location = NSEntityDescription.insertNewObject(forEntityName: "Location", into: self.persistentContainer.viewContext) as! Location
        location.name                       = name
        location.numberOfFloors             = Int32(numberOfFloors)
        location.numberOfAdditionalPlaces   = Int32(numberOfAdditionalPlaces)
        location.emails                     = andEmails
        
        for counter in 0..<numberOfFloors {
            let placement       = NSEntityDescription.insertNewObject(forEntityName: "Placement", into: self.persistentContainer.viewContext) as! Placement
            placement.number    = Int32(counter + 1)
            placement.name      = ""
            location.addToPlacements(placement)
        }
        
        for counter in 0..<numberOfAdditionalPlaces {
            let placement           = NSEntityDescription.insertNewObject(forEntityName: "Placement", into: self.persistentContainer.viewContext) as! Placement
            placement.number        = Int32(counter + 1)
            placement.isAdditional  = true
            placement.name          = ""
            location.addToPlacements(placement)
        }

        self.saveContext()
    }
    
    func editLocation(location: Location, name: String, numberOfFloors: Int, numberOfAdditionalPlaces:Int,  andEmails: String) {
        location.name       = name
        location.emails     = andEmails
        
        
        if location.numberOfFloors == Int32(numberOfFloors) && location.numberOfAdditionalPlaces == Int32(numberOfAdditionalPlaces) {
            return
        }
        
        location.numberOfFloors             = Int32(numberOfFloors)
        location.numberOfAdditionalPlaces   = Int32(numberOfAdditionalPlaces)
        
        location.removeFromPlacements(location.placements!)
        
        for counter in 0..<numberOfFloors {
            let placement       = NSEntityDescription.insertNewObject(forEntityName: "Placement", into: self.persistentContainer.viewContext) as! Placement
            placement.number    = Int32(counter + 1)
            placement.name      = ""
            location.addToPlacements(placement)
            
        }
        
        for counter in 0..<numberOfAdditionalPlaces {
            let placement           = NSEntityDescription.insertNewObject(forEntityName: "Placement", into: self.persistentContainer.viewContext) as! Placement
            placement.number        = Int32(counter + 1)
            placement.isAdditional  = true
            placement.name          = ""
            location.addToPlacements(placement) 
        }
        self.saveContext()
    }
    
    func prepareForNewShift(location: Location?) {
        if let placementsArray = location?.placements as? Set<Placement> {
            for placement in placementsArray {
                placement.events = nil
            }
        }
        self.saveContext()
    }
    
    func deleteLocation(location: Location?) {
        if let loc = location {
            self.persistentContainer.viewContext.delete(loc)
        }
        self.saveContext()
    }

    //MARK: - Event methods
    func createEvent(remark: String) -> Event {
        let event = NSEntityDescription.insertNewObject(forEntityName: "Event", into: self.persistentContainer.viewContext) as! Event
        event.remark        = remark
        return event
    }
    
    //MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MissionRevision2")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
