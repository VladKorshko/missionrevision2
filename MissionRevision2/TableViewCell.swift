//
//  TableViewCell.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 22/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit
import Foundation

class TableViewCell: UITableViewCell {
    
    var beginTimeActionBlock: (() -> Void)?
    var endTimeActionBlock: (() -> Void)?
    var remarkActionBlock: (() -> Void)?
    
    private var beginDateGestureRecognizer: UITapGestureRecognizer!
    private var endDateGestureRecognizer: UITapGestureRecognizer!
    private var remarkTextFieldGestureRecognizer: UITapGestureRecognizer!
    
    @IBOutlet private(set) weak var remarkLabel: UILabel!
    @IBOutlet private weak var beginningLabel: UILabel!
    @IBOutlet private weak var endLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.beginDateGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.beginTimeAction))
        self.endDateGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.endTimeAction))
        self.remarkTextFieldGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.remarkAction))
        
        self.beginningLabel.addGestureRecognizer(self.beginDateGestureRecognizer)
        self.endLabel.addGestureRecognizer(self.endDateGestureRecognizer)
        self.remarkLabel.addGestureRecognizer(self.remarkTextFieldGestureRecognizer)
        
        self.updateLabelsUI()
    }

    //MARK: - GestureRecognizer methods
    @objc private func beginTimeAction() {
        self.beginTimeActionBlock?()
    }
    
    @objc private func endTimeAction() {
        self.endTimeActionBlock?()
    }
    
    @objc private func remarkAction() {
        self.remarkActionBlock?()
    }
    
    //MARK: - Update with event
    func configureCellWithEvent(event: Event) {
        self.remarkLabel.text = event.remark
        
        if let date = event.beginning as Date? {
             self.beginningLabel.setTextWithAnimation(DateFormatter.stringFromDate(date: date))
        }
        
        if let date = event.end as Date? {
            self.endLabel.setTextWithAnimation(DateFormatter.stringFromDate(date: date))
        } 
        
        self.updateLabelsUI()
    }
    
    private func updateLabelsUI() {
        let beginingBorderColor: UIColor = self.beginningLabel.text?.isEmpty == true ? .defaultRedColor : .white
        let endBorderColor: UIColor = self.endLabel.text?.isEmpty == true ? .defaultRedColor : .white

        self.beginningLabel.layer.borderColor   = beginingBorderColor.cgColor
        self.endLabel.layer.borderColor         = endBorderColor.cgColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.remarkLabel.text = ""
        self.beginningLabel.text = ""
        self.endLabel.text = ""
    }
}
