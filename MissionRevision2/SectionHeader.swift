//
//  SectionHeader.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 21/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import Foundation
import UIKit

class SectionHeader: UICollectionReusableView {
    @IBOutlet weak var centralLabel: DefaultLabel!
}
