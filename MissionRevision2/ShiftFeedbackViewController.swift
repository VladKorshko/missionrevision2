//
//  ShiftFeedbackViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 02/04/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit

class ShiftFeedbackViewController: UIViewController {

    var shift: Shift?
    
    @IBOutlet weak var centralLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        self.centralLabel.text = self.shift?.historyCellTitleString()
        self.webView.loadHTMLString((self.shift?.createHTMLString())!, baseURL: nil)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }



    
}
