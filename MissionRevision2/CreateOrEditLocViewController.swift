//
//  CreateOrEditLocViewContoller.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 17/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit

private let kCreateLocationLabelText    = "Create new location"
private let kEditLocationLabelText      = "Edit location"
private let kEmptyFieldsWarn            = "Some fields are empty or incorrect"

class CreateOrEditLocViewContoller: UIViewController, UITextFieldDelegate {

    //MARK: - Properties
    private var location: Location?
    
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var numberOfFloorsTextField: UITextField!
    @IBOutlet private weak var numberOfAdditionalPlaces: UITextField!
    @IBOutlet private weak var emails: UITextField!
    @IBOutlet private weak var mainLabel: DefaultLabel!

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareLabels()
        self.defaultBackgroundImage()
    }
    
    //MARK: - Preparations
    private func prepareLabels() {
        if self.location == nil {
            self.mainLabel.text                 = kCreateLocationLabelText
        } else {
            self.mainLabel.text                 = kEditLocationLabelText
            self.nameTextField.text             = self.location!.name
            self.numberOfFloorsTextField.text   = self.location!.numberOfFloors.description
            self.numberOfAdditionalPlaces.text  =  self.location!.numberOfAdditionalPlaces.description
            self.emails.text                    = self.location!.emails
        }
    }
    
    //MARK: - Public methods
    func setLocation(_ location: Location?) {
        self.location = location
    }
    
    //MARK: - Actions
    @IBAction func doneButtonPressed(_ sender: Any) {
        if self.allTextFieldsValid() {
            let name                = self.nameTextField.text!
            let numberOfFloors      = Int(self.numberOfFloorsTextField.text!)
            let numberOfAddFloors   = Int(self.numberOfAdditionalPlaces.text!)
            let emails              = self.emails.text!

            if self.location == nil {
                DatabaseManager.sharedInstance.createLocationWith(name: name,
                                                                  numberOfFloors: numberOfFloors!,
                                                                  numberOfAdditionalPlaces: numberOfAddFloors!,
                                                                  andEmails: emails)
            } else {
                DatabaseManager.sharedInstance.editLocation(location: self.location!,
                                                            name: name,
                                                            numberOfFloors: numberOfFloors!,
                                                            numberOfAdditionalPlaces: numberOfAddFloors!,
                                                            andEmails: emails)
            }
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.showMessagePrompt(message: kEmptyFieldsWarn)
        }
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === self.nameTextField {
            self.numberOfFloorsTextField.becomeFirstResponder()
        } else if textField === self.numberOfFloorsTextField {
            self.numberOfAdditionalPlaces.becomeFirstResponder()
        } else if textField === self.numberOfAdditionalPlaces {
            self.emails.becomeFirstResponder()
        } else if textField === self.emails {
            self.doneButtonPressed(self)
        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - Validation
    private func allTextFieldsValid() -> Bool {
        return
                self.nameTextField.text!.isEmpty == false &&
                self.numberOfFloorsTextField.text!.isEmpty == false &&
                self.numberOfAdditionalPlaces.text!.isEmpty == false &&
                self.emails.text!.isEmpty == false &&
                self.isValidEmail(testStr: self.emails.text!)
    }
    
    private func isValidEmail(testStr:String) -> Bool {
        var isValid     = true
        let array       = testStr.arrayWithEmails()
        let emailRegEx  = "[ A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest   = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        for string in array {
            isValid = emailTest.evaluate(with: string)
        }
        return isValid
    }
}





















