//
//  DefaultEventViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 01/04/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit
import CoreData

class DefaultEventViewController: UIViewController, UITextViewDelegate {
    
    //MARK: - Properties
    private var location: Location?
    @IBOutlet private weak var textView: UITextView!
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultBackgroundImage()
    }
    
    //MARK: - Public methods
    func setLocation(_ location: Location?) {
        self.location = location
    }
    
    //MARK: - Actions
    @IBAction func doneButtonPressed() {
        let event = DatabaseManager.sharedInstance.createEvent(remark: self.textView.text)
        self.location?.addToEvents(event)
        self.navigationController!.popViewController(animated: true)
    }
    
    //MARK: - UITextViewDelegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
