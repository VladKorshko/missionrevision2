//
//  DefaultLabel.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 17/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import Foundation
import UIKit

class DefaultLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.masksToBounds    = true
        self.layer.cornerRadius     = 9
        self.backgroundColor        = UIColor.defaultButtonBackgroundColor
        self.font                   = UIFont.systemFont(ofSize: 21, weight: UIFontWeightSemibold)
    }
}
