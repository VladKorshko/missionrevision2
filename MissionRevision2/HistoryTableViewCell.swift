//
//  HistoryTableViewCell.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 02/04/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {


    @IBOutlet weak var centralLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWithShift(shift: Shift?) {
        if let string = shift?.historyCellTitleString() {
            self.centralLabel.text = string
        }
    }


}
