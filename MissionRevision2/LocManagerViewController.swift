//
//  LocManagerViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 17/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol LocationManagerViewControllerDelegate: class {
    func locationManagerVC(_ viewController: LocationManagerViewController, didSelect location: Location?)
    func locationManagerVC(_ viewController: LocationManagerViewController, didSetShiftProcess active: Bool)
    func locationManagerVC(_ viewController: LocationManagerViewController, didSetLocation isDefault: Bool)
}

private let kCreateLocationSegue        = "createLocationSegue"
private let kEditLocationSegue          = "editLocationSegue"
private let kHeightOfPickerComponent    = CGFloat(70.0)

class LocationManagerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, NSFetchedResultsControllerDelegate {
    
    //MARK: - Properties
    private var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    weak var delegate: LocationManagerViewControllerDelegate?
    
    @IBOutlet private weak var locationPicker: UIPickerView!

    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeFetchedResultsController()
        self.defaultBackgroundImage()
    }
    
    //MARK: - Preparations
    func initializeFetchedResultsController() {
        let request                 = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
        let defaultSort             = NSSortDescriptor(key: "isDefaultLocation" , ascending: false)
        let nameSort                = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors     = [defaultSort, nameSort]
        
        let moc = DatabaseManager.sharedInstance.persistentContainer.viewContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    
    //MARK: - Actions
    @IBAction private func editButtonPressed() {
        if self.getCurrentLocation() != nil {
            self.performSegue(withIdentifier: kEditLocationSegue, sender: nil)
        }
    }
    @IBAction private func deleteButtonPressed() {
        if let location = self.getCurrentLocation() {
            if location.isDefaultLocation {
                self.delegate?.locationManagerVC(self, didSelect: nil)
                self.delegate?.locationManagerVC(self, didSetShiftProcess: false)
            }
            
            DatabaseManager.sharedInstance.deleteLocation(location: location)
        }
    }
    
    @IBAction func selectButtonPressed() {
        self.delegate?.locationManagerVC(self, didSetLocation: false)
        
        if let location = self.getCurrentLocation() {
            location.isDefaultLocation = true
            self.delegate?.locationManagerVC(self, didSelect: location)
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    private func getCurrentLocation() -> Location? {
        if let count = self.fetchedResultsController.fetchedObjects?.count, count > 0 {
            let elementIndex = self.locationPicker.selectedRow(inComponent: 0)
            let location = self.fetchedResultsController.fetchedObjects![elementIndex] as! Location
            return location
        } else {
            return nil
        }
    }

    //MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return kHeightOfPickerComponent
    }

    //MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (self.fetchedResultsController.fetchedObjects?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        let fetchedLocation     = self.fetchedResultsController.fetchedObjects?[row] as? Location
        let textString          = fetchedLocation?.name
        let title               = NSAttributedString(string: textString ?? "-",
                                                     attributes:
            [NSFontAttributeName: UIFont.systemFont(ofSize: 36, weight: UIFontWeightRegular),
             NSForegroundColorAttributeName: UIColor.white])
        label!.attributedText   = title
        label!.textAlignment    = .center
        return label!
    }

    
    //MARK: - Segues
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == kEditLocationSegue && self.getCurrentLocation() == nil {
            return false
        }
        return true
    }
    
    override func prepare(for segue: (UIStoryboardSegue!), sender: Any?) {
        if (segue.identifier == kEditLocationSegue) {
            let targetController = segue.destination as! CreateOrEditLocViewContoller
            targetController.setLocation(getCurrentLocation())
        }
    }
    
    //MARK: - Fetched result controller
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.locationPicker.reloadAllComponents()
    }
}



