//
//  ComposeEventViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 22/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit
import CoreData
import Foundation

let kAddEventSegue = "addEventSegue"

class ComposeEventViewController: UIViewController, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, NSFetchedResultsControllerDelegate {
    
    //MARK: - Properties
    private var placement: Placement?
    private var selectedTextField: UITextField?
    private var eventPicker: UIPickerView?
    private var toolbar: UIToolbar?
    private var gestRecognizer: UITapGestureRecognizer?
    private var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    
    @IBOutlet private weak var beginningTextField: UITextField!
    @IBOutlet private weak var endingTextField: UITextField!
    @IBOutlet private weak var textView: UITextView!

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeFetchedResultsController()
        self.defaultBackgroundImage()
        
        self.beginningTextField.text                = DateFormatter.stringFromDate(date: Date())
        
        self.beginningTextField.inputAccessoryView  = self.loadDatePickerToolbar()
        self.endingTextField.inputAccessoryView     = self.loadDatePickerToolbar()
        
        let datePickerView: UIDatePicker    = self.loadDatePicker()
        
        self.beginningTextField.inputView   = datePickerView
        self.endingTextField.inputView      = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
        
        self.createEventPicker()
        self.createToolbar()
        self.configureGestRecognizer()
    }

    //MARK: - Preparations
    func initializeFetchedResultsController() {
        let request: NSFetchRequest<NSFetchRequestResult> = Event.fetchRequest()
        let dateSort                = NSSortDescriptor(key: "remark", ascending: true)
        let predicate               = NSPredicate(format: "location == %@", (self.placement?.location)!)
        request.sortDescriptors     = [dateSort]
        request.predicate           = predicate
        
        let moc = DatabaseManager.sharedInstance.persistentContainer.viewContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    private func configureGestRecognizer() {
        let gestRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hidePicker))
        self.view.addGestureRecognizer(gestRecognizer)
        
        self.gestRecognizer = gestRecognizer
        self.gestRecognizer!.isEnabled  = false
    }
    
    private func createEventPicker() {
        let pickerStartFrame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height / 3)
        let picker              = UIPickerView(frame: pickerStartFrame)
        picker.delegate         = self
        picker.dataSource       = self
        picker.backgroundColor  = UIColor.lightGray
        
        self.view.addSubview(picker)
        self.eventPicker = picker
    }
    
    private func createToolbar() {
        let startFrame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: 40)
        let toolbar =  UIToolbar(frame: startFrame)
        
        toolbar.barStyle        = UIBarStyle.blackTranslucent
        toolbar.tintColor       = UIColor.defaultBlueColor
        toolbar.backgroundColor = UIColor.defaultButtonBackgroundColor
        
        let selectButton        = UIButton()
        selectButton.frame      = CGRect(x: 0, y: 0, width: 70, height: 40)
        selectButton.addTarget(self, action: #selector(self.selectEventButtonPressed), for: UIControlEvents.touchUpInside)
        selectButton.backgroundColor    = UIColor.clear
        selectButton.setTitle("Select", for: UIControlState.normal)
        selectButton.titleLabel?.font   = UIFont.systemFont(ofSize: 17, weight: UIFontWeightSemibold)
        selectButton.setTitleColor(UIColor.defaultBlueColor, for: UIControlState.normal)
        selectButton.setTitleColor(UIColor.darkGray, for: UIControlState.highlighted)
        
        let selectEventBarButton    = UIBarButtonItem(customView: selectButton)
        let addEventBarButton       = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(self.addEventBarButtonPressed))
        let deleteEventBarButton    = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(self.deleteEventBarButtonPressed))
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([deleteEventBarButton, flexSpace, selectEventBarButton, flexSpace, addEventBarButton], animated: true)
        self.view.addSubview(toolbar)
        
        self.toolbar = toolbar
    }
    
    //MARK: - Public methods
    func setPlacement(_ placement: Placement?) {
        self.placement = placement
    }
    
    //MARK: - Actions
    @IBAction private func doneButtonPressed(_ sender: Any) {
        let event           = DatabaseManager.sharedInstance.createEvent(remark: self.textView.text)
        
        let date            = DateFormatter.dateFromString(string: self.beginningTextField.text!)
        event.beginning     = date as NSDate?
        
        let date2           = DateFormatter.dateFromString(string: self.endingTextField.text!)
        event.end           = date2 as NSDate?
        
        self.placement?.addToEvents(event)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func textFieldPressed(_ sender: UITextField) {
        self.hidePicker()
        self.selectedTextField           = sender
        self.selectedTextField?.text     = DateFormatter.stringFromDate(date: Date())
    }
    
    @IBAction private func eventButtonPressed() {
        self.showPicker()
    }
    
    private func showPicker() {
        self.view.endEditing(true)
        self.gestRecognizer?.isEnabled = true
        UIView.animate(withDuration: 0.25) {
            self.eventPicker?.frame = CGRect(x: 0, y: self.view.bounds.height - self.view.bounds.height / 3.1, width: self.view.bounds.width, height: self.view.bounds.height / 3.1)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.bounds.height - self.view.bounds.height / 3.1 - 40, width: self.view.bounds.width, height: 40)
            
        }
    }
    
    @objc private func hidePicker() {
        self.gestRecognizer?.isEnabled = false
        UIView.animate(withDuration: 0.25, animations: {
            self.eventPicker?.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height / 3.1)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: 40)
        })
    }
    
    @objc private func selectEventButtonPressed() {
        if (self.eventPicker?.numberOfRows(inComponent: 0))! > 0 {
            let selectedRowIndex    = (self.eventPicker?.selectedRow(inComponent: 0))!
            let event               = self.fetchedResultsController.fetchedObjects?[selectedRowIndex] as? Event
            let string              = (event?.remark)!
            self.textView.text.append(string)
        }
        self.hidePicker()
    }
    
    @objc private func addEventBarButtonPressed() {
        self.performSegue(withIdentifier: kAddEventSegue, sender: nil)
    }
    
    override func toolbarDonePressed(_ sender: UIBarButtonItem) {
        self.selectedTextField?.resignFirstResponder()
    }
    
    @objc private func deleteEventBarButtonPressed() {
        if (self.eventPicker?.numberOfRows(inComponent: 0))! > 0 {
            let selectedRowIndex = (self.eventPicker?.selectedRow(inComponent: 0))!
            if let event = self.fetchedResultsController.fetchedObjects?[selectedRowIndex] as? Event {
                self.placement?.location?.removeFromEvents(event)
            }
        }
    }
    
    //MARK: - Date picker
    func datePickerValueChanged(sender: UIDatePicker) {
        self.selectedTextField?.text = DateFormatter.stringFromDate(date: sender.date)
    }

   //MARK: - UITextViewDelegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.hidePicker()
        return true
    }
    
    //MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as? DefaultEventViewController
        targetController?.setLocation(self.placement?.location)
    }
    
    //MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (self.fetchedResultsController.fetchedObjects?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let event = self.fetchedResultsController.fetchedObjects?[row] as? Event
        return event?.remark
    }
    
    
    //MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 33.0
    }
    
    
    //MARK: - Fetched result controller
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.eventPicker?.reloadAllComponents()
    }
}
