//
//  PlacementViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 21/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit
import CoreData
import Foundation

private let kDefaultTableViewCell   = "defaultTableViewCell"
private let kTableViewCellHeight    = CGFloat(60.0)
private let kEventString            = "Edit event:"

enum EventTimeType {
    case beginning
    case ending
}

class PlacementViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    //MARK: - Properties
    private var placement: Placement?
    private var picker: UIDatePicker?
    private var selectedEvent: Event?
    private var toolbar: UIToolbar?
    private var gestRecognizer: UIGestureRecognizer?
    private var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var switchTumbler: UISwitch!
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.defaultBackgroundImage()
        self.initializeFetchedResultsController()
        
        self.nameTextField.text = self.placement?.name
        self.switchTumbler.setOn((self.placement?.isIgnored)!, animated: true)

        self.createDatePicker()
        self.createToolbar()
        self.createDatePicker()
        self.configureGestRecognizer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.hidePicker()
    }
    
    //MARK: - Preparations
    func initializeFetchedResultsController() {
        let request: NSFetchRequest<NSFetchRequestResult> = Event.fetchRequest()
        
        let dateSort                = NSSortDescriptor(key: "beginning", ascending: true)
        let predicate               = NSPredicate(format: "placement == %@", self.placement!)
        request.sortDescriptors     = [dateSort]
        request.predicate           = predicate
        
        let moc = DatabaseManager.sharedInstance.persistentContainer.viewContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    private func configureGestRecognizer() {
        let gestRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hidePicker))
        self.view.addGestureRecognizer(gestRecognizer)
        
        self.gestRecognizer             = gestRecognizer
        self.gestRecognizer!.isEnabled  = false
    }
    
    private func createDatePicker() {
        let pickerStartFrame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height / 3)
        let datePickerView: UIDatePicker    = self.loadDatePicker()
        datePickerView.frame                = pickerStartFrame
        
        self.view.addSubview(datePickerView)
        self.picker  =  datePickerView
    }
    
    private func createToolbar() {
        let toolbar = self.loadDatePickerToolbar()
        let toolbarStartFrame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: 40)
        toolbar.frame = toolbarStartFrame
        
        self.view.addSubview(toolbar)
        self.toolbar = toolbar
    }

    
    //MARK: - Actions
    @IBAction func doneButtonPressed() {
        self.placement?.name            = self.nameTextField.text
        self.placement?.isIgnored       = self.switchTumbler.isOn
        DatabaseManager.sharedInstance.saveContext()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Public methods
    func setPlacement(_ placement: Placement?) {
        self.placement = placement
    }
    
    //MARK: - Date Picker methods
    private func showPicker() {
        self.view.endEditing(true)
        self.gestRecognizer!.isEnabled = true
        
        UIView.animate(withDuration: 0.25) {
            self.picker!.frame = CGRect(x: 0, y: self.view.bounds.height - self.view.bounds.height / 3, width: self.view.bounds.width, height: self.view.bounds.height / 3)
            self.toolbar!.frame = CGRect(x: 0, y: self.view.bounds.height - self.view.bounds.height / 3 - 40, width: self.view.bounds.width, height: 40)
        }
    }
    
    @objc private func hidePicker() {
        self.gestRecognizer!.isEnabled = false
        
        UIView.animate(withDuration: 0.25, animations: {
            self.picker!.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height / 3)
            self.toolbar!.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: 40)
        })
    }
    
    override func toolbarDonePressed(_ sender: UIBarButtonItem) {
        if self.picker!.cellEventTimeType == .beginning {
            self.selectedEvent!.beginning = self.picker?.date as NSDate?
        } else if self.picker!.cellEventTimeType == .ending {
            self.selectedEvent!.end = self.picker?.date as NSDate?
        }
        self.hidePicker()
    }

    private func showPickerWithType(type: EventTimeType, indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! TableViewCell
        cell.setSelected(true, animated: true)
        
        let event                       = self.fetchedResultsController.fetchedObjects?[indexPath.row] as! Event
        
        self.selectedEvent              = event
        self.picker!.date               = Date()
        self.picker!.cellEventTimeType  = type
        self.showPicker()
        
        cell.setSelected(false, animated: true)
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.fetchedResultsController.fetchedObjects?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: kDefaultTableViewCell, for: indexPath) as! TableViewCell
        let event = self.fetchedResultsController.fetchedObjects?[indexPath.row] as! Event

        cell.configureCellWithEvent(event: event)

        cell.beginTimeActionBlock = { [weak self] in
            let newIndexPath = self!.tableView.indexPath(for: cell)!
            self!.showPickerWithType(type: .beginning, indexPath: newIndexPath)
        }
        
        cell.endTimeActionBlock = { [weak self] in
            let newIndexPath = self!.tableView.indexPath(for: cell)!
            self!.showPickerWithType(type: .ending, indexPath: newIndexPath)
        }
        
        cell.remarkActionBlock = { [weak self] in
            cell.setSelected(true, animated: true)
            self!.showTextInputPromptWithMessage(message: kEventString,
                                                 textFieldText: cell.remarkLabel.text!,
                                                 secure: false,
                                                 completion:
                { (pressedOK, input) in
                if pressedOK {
                    event.remark = input
                }
            })
            cell.setSelected(false, animated: true)
        }
        
        return cell
    }


    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let context = self.fetchedResultsController.managedObjectContext
            context.delete(self.fetchedResultsController.object(at: indexPath) as! NSManagedObject)
            DatabaseManager.sharedInstance.saveContext()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kTableViewCellHeight
    }

    //MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as! ComposeEventViewController
        targetController.setPlacement(self.placement)
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }

    //MARK: - NSFetchedResultsControllerDelegate
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        let event = anObject as? Event

        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                let cell = self.tableView.cellForRow(at: indexPath) as! TableViewCell
                cell.configureCellWithEvent(event: event!)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }

    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.hidePicker()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
