//
//  ShiftViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 20/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import MessageUI

private let kCollecionViewCellReuseIdentifier       = "collecionViewCellReuseIdentifier"
private let kCollectionSectionHeaderReusableView    = "collectionReusableView"
private let kAdditionalPlacesSectionHeader          = "Additional places"
private let kPlacementSegue                         = "placementSegue"
private let kChooseLocationMessage                  = "Please select a location"

private let kCantSendMails                          = "Set up an email account"
private let kNotFinishedEvents                      = "Some events are not finished"

private let kFloorsItemsPerRow: CGFloat             = 2
private let kAdditionalItemsPerRow: CGFloat         = 4

private let kInsetForFloorsItem: CGFloat            = 20
private let kInsetForAdditionalItem: CGFloat        = 20

private let kInsetForSection: CGFloat               = 20


protocol ShiftViewControllerDelegate: class {
    func shiftViewController(_ viewController: ShiftViewController, didSetShiftProcess active: Bool)
}

class ShiftViewController: UICollectionViewController, NSFetchedResultsControllerDelegate, UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate {
    
    //MARK: - Stored Properties
    weak var delegate: ShiftViewControllerDelegate?
    private var shift: Shift?
    private var dataBuffer: [[Placement]]?
    private var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!

    //MARK: - Computed Properties
    var floorsCellSize: CGSize {
        let totalInsetSpace = kInsetForFloorsItem * (kFloorsItemsPerRow + 1)
        let spaceForItems = UIScreen.main.bounds.size.width - totalInsetSpace
        let sideSize = spaceForItems / kFloorsItemsPerRow
        return CGSize(width: sideSize, height: sideSize)
    }
    
    var additionalCellSize: CGSize {
        let totalInsetSpace = kInsetForAdditionalItem * (kAdditionalItemsPerRow + 1)
        let spaceForItems = UIScreen.main.bounds.size.width - totalInsetSpace
        let sideSize = spaceForItems / kAdditionalItemsPerRow
        return CGSize(width: sideSize, height: sideSize)
    }

    //MARK: - Life cycle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeFetchedResultsController()
        
        if self.fetchedResultsController.fetchedObjects!.count > 0 {
            self.shift = self.fetchedResultsController.fetchedObjects![0] as? Shift
        } else {
            self.shift = DatabaseManager.sharedInstance.createShift()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.backgroundView = UIImageView.init(image: UIImage.init(named: "map"))
        self.navigationItem.title   = self.shift?.location?.name
        self.dataBuffer             = self.createDataBuffer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView!.reloadData()
    }
    
    //MARK: - Preparations
    private func createDataBuffer() -> [[Placement]] {
        var dataBuffer: [[Placement]] = []
        
        if let tempArray = self.shift?.location?.placements?.allObjects as? [Placement] {
            var arrayWithFloors     = tempArray.filter({ !$0.isAdditional })
            arrayWithFloors         = arrayWithFloors.sorted(by: { $0.number < $1.number })
            
            var arrayWithAdditionaPlaces    = tempArray.filter({ $0.isAdditional} )
            arrayWithAdditionaPlaces        = arrayWithAdditionaPlaces.sorted(by: { $0.number < $1.number })
            
            dataBuffer.append(arrayWithFloors)
            dataBuffer.append(arrayWithAdditionaPlaces)
        }
        
        return dataBuffer
    }
    
    //MARK: - Public methods
    func setLocation(location: Location?) {
        DatabaseManager.sharedInstance.prepareForNewShift(location: location)
        self.shift?.location = location
        self.shift?.date     = NSDate()
    }
    
    //MARK: - Actions
    @IBAction private func mailButtonPressed() {
        if MFMailComposeViewController.canSendMail() {
            if self.shift?.isReadyToSendEmail() ?? false {
                UINavigationBar.appearance().barTintColor = UIColor.white

                let mailComposeVC                   = MFMailComposeViewController()
                mailComposeVC.mailComposeDelegate   = self
                let string                          = self.shift?.getHTMLString()
                
                mailComposeVC.setSubject(self.shift!.getSubjectString())
                mailComposeVC.setMessageBody(string!, isHTML: true)
                mailComposeVC.setToRecipients(self.shift!.location?.emails?.arrayWithEmails())

                self.present(mailComposeVC, animated: true, completion: nil)
            } else {
                self.showMessagePrompt(message: kNotFinishedEvents)
            }
         } else {
            self.showMessagePrompt(message: kCantSendMails)
        }
    }

    //MARK: - UICollectionViewDataSource methods
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.dataBuffer!.count
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataBuffer![section].count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let placement = self.dataBuffer![indexPath.section][indexPath.row]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCollecionViewCellReuseIdentifier, for: indexPath) as! CollectionViewCell
        cell.configureCellWithPlacement(placement: placement)
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusalbeView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: kCollectionSectionHeaderReusableView, for: indexPath) as! SectionHeader
        
        if indexPath.section == 0 {
            let dateformatter        = DateFormatter()
            dateformatter.dateFormat = "E MMM dd, yyyy"
            
            if let date = self.shift!.date {
                reusalbeView.centralLabel.text = dateformatter.string(from: date as Date)
            }
        } else if indexPath.section == 1 {
            reusalbeView.centralLabel.text = kAdditionalPlacesSectionHeader
        }
        
        return reusalbeView
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return indexPath.section == 0 ? self.floorsCellSize : self.additionalCellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(kInsetForSection, kInsetForSection, kInsetForSection, kInsetForSection)
    }
    
    //MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kPlacementSegue {
            if let indexPath = self.collectionView?.indexPath(for: (sender as? CollectionViewCell)!) {
                let placement             = self.dataBuffer?[indexPath.section][indexPath.row]
                let tagetController       = segue.destination as? PlacementViewController
                tagetController?.setPlacement(placement)
            }
        }
    }
    
    //MARK: - Fetched result controller
    func initializeFetchedResultsController() {
        let request                 = NSFetchRequest<NSFetchRequestResult>(entityName: "Shift")
        let dateSort                = NSSortDescriptor(key: "date", ascending: true)
        request.sortDescriptors     = [dateSort]
        
        let moc = DatabaseManager.sharedInstance.persistentContainer.viewContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    
    //MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        UINavigationBar.appearance().barTintColor = UIColor.defaultNavBarBackgroundColor
        controller.dismiss(animated: true, completion: nil)
        switch result {
            case .sent:
                self.showMessagePrompt(message: "E-mail was sent successfully");
                self.delegate?.shiftViewController(self, didSetShiftProcess: false)
            case .saved:
                self.showMessagePrompt(message: "E-mail was saved successfully")
            case .cancelled:
                self.showMessagePrompt(message: "E-mail was cancelled")
            case .failed:
                self.showMessagePrompt(message: "E-mail delivery failed")
        }
    }

}
