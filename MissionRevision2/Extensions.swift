//
//  Extensions.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 17/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UIDatePicker extension
var newProperty: EventTimeType = .beginning

extension UIDatePicker {
    var cellEventTimeType: EventTimeType {
        get {
            return (objc_getAssociatedObject(self, &newProperty) as? EventTimeType)!
        }
        set {
            objc_setAssociatedObject(self, &newProperty, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

//MARK: - String extension
extension String {
    func arrayWithEmails() -> [String] {
        let charStringForSet    = ",/;"
        let charSet             = CharacterSet(charactersIn: charStringForSet)
        let array               = self.components(separatedBy: charSet)
        
        return array
    }
}

//MARK: - DateFormatter extension
extension DateFormatter {
    static func stringFromDate(date: Date) -> String  {
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "H:mm"
        return dateFormatter.string(from:date)
    }
    
    static func dateFromString(string: String) -> Date? {
        let currentDate                 = Date()
        let bufferDateFormatter         = DateFormatter()
        bufferDateFormatter.dateFormat  = "yyyy MMMM dd "
        
        var bufferDateString            = bufferDateFormatter.string(from: currentDate)
        bufferDateString.append(string)
        
        let dateFormatter               = DateFormatter()
        dateFormatter.dateFormat        = "yyyy MMMM dd H:mm"
        return dateFormatter.date(from: bufferDateString)
    }
}

//MARK: - UIColor extension
extension UIColor {
    static var defaultNavBarTitleColor: UIColor {
        return UIColor.init(red: 116/255, green: 191/255, blue: 255/255, alpha: 1)
    }
    
    static var defaultNavBarBackgroundColor: UIColor {
        return UIColor.init(red: 43/255, green: 43/255, blue: 43/255, alpha: 1)
    }
    
    static var defaultButtonBackgroundColor: UIColor {
        return UIColor.init(red: 43/255, green: 43/255, blue: 43/255, alpha: 1)
    }
    
    static var defaultBlueColor: UIColor {
        return UIColor.init(red: 116/255, green: 191/255, blue: 255/255, alpha: 1)
    }
    
    static var defaultRedColor: UIColor {
        return UIColor.init(red: 233/255, green: 47/255, blue: 70/255, alpha: 1)
    }
    
    static var selectedCellColor: UIColor {
        return UIColor.init(red: 233/255, green: 47/255, blue: 70/255, alpha: 1)
    }
    
    static var backgroundCellGreenColor: UIColor {
        return UIColor.init(red: 122/255, green: 191/255, blue: 27/255, alpha: 1)
    }
}

//MARK: - UIViewController extension
extension UIViewController {
    func defaultBackgroundImage() {
        if let image = UIImage.init(named: "map") {
            self.view.backgroundColor = UIColor.init(patternImage: image)
        }
    }

    func showMessagePrompt(message: String) {
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)

        let alertContoller = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertContoller.addAction(okAction)
        
        self.present(alertContoller, animated: true, completion: nil)
    }
    
    func showTextInputPromptWithMessage(message: String, textFieldText: String, secure: Bool, completion: @escaping (Bool, String?) -> ()) {
        let alerController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) in
            completion(false, nil)
        }
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { [weak alerController] (action) in
            completion(true, alerController!.textFields?[0].text)
        }
        
        alerController.addTextField { $0.isSecureTextEntry = secure; $0.text = textFieldText }
        alerController.addAction(okAction)
        alerController.addAction(cancelAction)

        self.present(alerController, animated: true, completion: nil)
    }

    //DatePicker
    func loadDatePicker() -> UIDatePicker {
        let datePickerView: UIDatePicker    = UIDatePicker()
        datePickerView.datePickerMode       = UIDatePickerMode.time
        datePickerView.locale               = Locale(identifier: "en_GB")
        datePickerView.backgroundColor      = UIColor.lightGray
        
        return datePickerView
    }
    
    //Toolbar
    func loadDatePickerToolbar() -> UIToolbar {
        let toolBar = UIToolbar(frame: CGRect(x: 0,
                                              y: 0,
                                              width: 0,
                                              height: 40.0))
        toolBar.barStyle        = UIBarStyle.blackTranslucent
        toolBar.tintColor       = UIColor.defaultBlueColor
        toolBar.backgroundColor = UIColor.defaultButtonBackgroundColor

        let button              = UIButton()
        button.frame            = CGRect(x: 0, y: 0, width: 50, height: 40)
        button.backgroundColor  = UIColor.clear
        button.addTarget(self, action: #selector(self.toolbarDonePressed(_:)), for: UIControlEvents.touchUpInside)
        
        button.setTitle("Done", for: UIControlState.normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightSemibold)
        button.setTitleColor(UIColor.defaultBlueColor, for: UIControlState.normal)
        button.setTitleColor(UIColor.darkGray, for: UIControlState.highlighted)

        let DoneBarBtn          = UIBarButtonItem(customView: button)
        let flexSpace           = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let label               = UILabel(frame: CGRect(x: 0,
                                                        y: 0,
                                                        width: self.view.frame.size.width / 3,
                                                        height: 40.0))
        label.font              = UIFont.systemFont(ofSize: 15, weight: UIFontWeightSemibold)
        label.backgroundColor   = UIColor.clear
        label.textColor         = UIColor.white
        label.text              = "Select time"
        label.textAlignment     = NSTextAlignment.center
        
        let textBtn = UIBarButtonItem(customView: label)
        
        toolBar.setItems([flexSpace,textBtn,flexSpace,DoneBarBtn], animated: true)
        
        return toolBar
    }
    
    func toolbarDonePressed(_ sender: UIBarButtonItem) {}
}

extension Shift {
    func getSubjectString() -> String {
        var subjectString = "Feedback for: "
        if let locationName = self.location?.name {
            subjectString += locationName
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d, yyyy"
        subjectString += ", date: " + formatter.string(from: self.date! as Date)
        
        return subjectString
    }

    func getHTMLString() -> String {
        //Table header
        var tableString = "<html><body>"
        tableString += "<h1 style='text-align: center;color:black;'>\(self.getSubjectString())</h1>"

        //Table style
        tableString += "<table style='width: 100%; border-collapse: collapse; font-family: arial, sans-serif; border: 2px solid rgb(71,73,73); color: black;'>"

        //Create sorted array
        var bufferArray = self.location?.placements?.allObjects as! [Placement]
        
        var arrayOfAdditionals = bufferArray.filter { (placement) -> Bool in
            return placement.isAdditional
        }
        arrayOfAdditionals.sort { (placement1, placement2) -> Bool in
            return placement1.number < placement2.number
        }
        
        var arrayOfNotAdditionals = bufferArray.filter { (placement) -> Bool in
            return !placement.isAdditional
        }
        arrayOfNotAdditionals.sort { (placement1, placement2) -> Bool in
            return placement1.number < placement2.number
        }
        
        bufferArray = arrayOfNotAdditionals
        bufferArray.append(contentsOf: arrayOfAdditionals)
        
        //Show titles
        var tempString = ""
        for placement in bufferArray {
            if placement.isIgnored {
                tempString = "N\\N"
            } else if placement.isAdditional {
                tempString = "Additional \(placement.number)"
            } else {
                tempString = "\(placement.number)"
            }
        
        
            tableString += "<tr><th colspan='3' style='font-size:18px; text-align: center; border: 2px solid rgb(71,73,73); background-color:powderblue;'>\(tempString)"

            if let empty = placement.name?.isEmpty, empty == true || placement.isIgnored {
                tableString += "</th></tr>"
            } else {
                tableString += ": \(placement.name!)</th></tr></b>"
            }

            //Event/Start/End headers
            if let count = placement.events?.count, count > 0 {
                tableString += "<tr>" +
                "<th style='border: 1px solid rgb(71,73,73);'>Event</th>" +
                "<th style='border: 1px solid rgb(71,73,73);'>Start time</th>" +
                "<th style='border: 1px solid rgb(71,73,73);'>End time</th>" +
                "</tr>"
            }
            
            var arrayWithEvents = placement.events?.allObjects as! [Event]
            arrayWithEvents.sort(by: { (event1, event2) -> Bool in
                return event1.beginning! as Date > event2.beginning! as Date
            })

            //Event/Start/End data
            for event in arrayWithEvents {
                let eventBeginning = DateFormatter.stringFromDate(date: event.beginning! as Date)
                let eventEnd = DateFormatter.stringFromDate(date: event.end! as Date)

                tempString = "<tr>" +
                    "<td style='border: 1px solid rgb(71,73,73);'>\(event.remark!)</td>" +
                    "<td style='border: 1px solid rgb(71,73,73);text-align: center;'>\(eventBeginning)</td>" +
                    "<td style='border: 1px solid rgb(71,73,73);text-align: center;'>\(eventEnd)</td>" +
                "</tr>"
                
                tableString += tempString
            }
        }
        tableString += "</table></body></html>"
        
        return tableString
    }
    
    func isReadyToSendEmail() -> Bool {
        var isReady = true
        if let placementSet = self.location?.placements as? Set<Placement> {
            for placement in placementSet {
                if let eventsSet = placement.events as? Set<Event> {
                    for event in eventsSet {
                        if event.end == nil {
                            isReady = false
                        }
                    }
                }
            }
        }
        return isReady
    }
}

//MARK: - UILabel extension
extension UILabel {
    func setTextWithAnimation(_ text: String) {
        let animation               = CATransition()
        animation.timingFunction    = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.type              = kCATransitionFade
        animation.duration          = 1
        self.layer.add(animation, forKey: "kCATransitionFade")
        self.text = text
    }
}

