//
//  MainViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 17/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit
import Foundation

private let kCurrentShiftIsNotActive    = "You need to start a new shift"
private let kLocationIsNotSelectedText  = "Location is not selected\n You may choose one in location manager"
private let kLocationMangerSegue        = "locationManagerSegue"
private let kCurrentShiftSegue          = "currentShiftSegue"
private let kNewShiftSegue              = "newShiftSegue"

class MainViewController: UIViewController, LocationManagerViewControllerDelegate, ShiftViewControllerDelegate {

    //MARK: - Properties
    private var defaultLocation: Location?
    private var shiftIsInProcess: Bool = false
    
    @IBOutlet private weak var centralLabel: DefaultLabel!
    @IBOutlet private weak var locationSelectionIndicator: UIImageView!

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultBackgroundImage()
        self.configureCentralLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareLocationIndicatorImg()
    }
    
    //MARK: - Preparations
    func configureCentralLabel() {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "EEEE, MMM d, yyyy"
        self.centralLabel.text  =  formatter.string(from: Date())
    }
    
    func prepareLocationIndicatorImg() {
        if self.defaultLocation == nil {
            self.locationSelectionIndicator.image = UIImage(named: "checkin_failed")
        } else {
            self.locationSelectionIndicator.image = UIImage(named: "checkin_done")
            self.locationSelectionIndicator.alpha = 0
            
            UIView.animate(withDuration: 1, animations: {
                self.locationSelectionIndicator.alpha = 1;
            });
        }
    }
    //MARK: - Segues
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == kNewShiftSegue && self.defaultLocation == nil {
            self.showMessagePrompt(message: kLocationIsNotSelectedText)
            return false
        } else if identifier == kCurrentShiftSegue && self.shiftIsInProcess == false {
            self.showMessagePrompt(message: kCurrentShiftIsNotActive)
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kLocationMangerSegue {
            let targetController = segue.destination as! LocationManagerViewController
            targetController.delegate = self
        } else if segue.identifier == kNewShiftSegue {
            let targetController = segue.destination as! ShiftViewController
            targetController.setLocation(location: self.defaultLocation)
            
            self.shiftIsInProcess     = true
            self.defaultLocation      = nil
            targetController.delegate = self
        }
    }
    
    //MARK: - LocationTransferDelegate
    func locationManagerVC(_ viewController: LocationManagerViewController, didSelect location: Location?) {
        self.defaultLocation = location
    }
    func locationManagerVC(_ viewController: LocationManagerViewController, didSetShiftProcess active: Bool) {
        self.shiftIsInProcess = active

    }
    func locationManagerVC(_ viewController: LocationManagerViewController, didSetLocation isDefault: Bool) {
        self.defaultLocation?.isDefaultLocation = isDefault
    }
    
    //MARK: - ShiftViewControllerDelegate
    func shiftViewController(_ viewController: ShiftViewController, didSetShiftProcess active: Bool) {
        self.shiftIsInProcess = active
    }
}


