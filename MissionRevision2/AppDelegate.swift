//
//  AppDelegate.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 17/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle                 = UIStatusBarStyle.lightContent
        UINavigationBar.appearance().tintColor              = UIColor.white
        UINavigationBar.appearance().barTintColor           = UIColor.defaultNavBarBackgroundColor
        UINavigationBar.appearance().titleTextAttributes    = [NSForegroundColorAttributeName : UIColor.defaultNavBarTitleColor, NSFontAttributeName: UIFont.systemFont(ofSize: 24)]
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        DatabaseManager.sharedInstance.saveContext()
    }
}

