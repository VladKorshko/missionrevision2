//
//  CollectionViewCell.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 20/03/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import Foundation
import UIKit

private let kCharStringForSet = ",;/"
private let kNotNeededString  = "N/N"

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var centralLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius                      = 9
        self.backgroundView                          = UIView()
        self.selectedBackgroundView                  = UIView()
        self.backgroundView?.backgroundColor         = UIColor.backgroundCellGreenColor
        self.selectedBackgroundView?.backgroundColor = UIColor.defaultRedColor
    }

    func configureCellWithPlacement(placement: Placement) {
        self.centralLabel.text = String(placement.number)
        
        if let placementName = placement.name {
            let placementNames = placementName.components(separatedBy: CharacterSet(charactersIn: kCharStringForSet))
            self.descriptionLabel.text  = placementNames.joined(separator: "\n")
        }

        if placement.isIgnored {
            self.centralLabel.text                  = kNotNeededString
            self.backgroundView?.backgroundColor    = UIColor.defaultButtonBackgroundColor
        }
        
        if let eventsSet = placement.events as? Set<Event> {
            for event in eventsSet {
                if event.end == nil {
                    self.backgroundView?.backgroundColor = UIColor.defaultRedColor
                }
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.backgroundView?.backgroundColor = UIColor.backgroundCellGreenColor
    }
}
