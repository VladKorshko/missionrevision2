//
//  ShiftListViewController.swift
//  MissionRevision2
//
//  Created by Vlad Korshko on 02/04/2017.
//  Copyright © 2017 Vlad Korshko. All rights reserved.
//

import UIKit
import CoreData

fileprivate let kHistoryTableViewCell   = "HistoryTableViewCell"
fileprivate let kShiftFeebackSegue      = "shiftFeebackSegue"

class ShiftHistoryViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK: - Properties
    var selectedShift: Shift?
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultBackgroundImage()
        self.initializeFetchedResultsController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.fetchedResultsController.fetchedObjects?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: kHistoryTableViewCell) as? HistoryTableViewCell {
            
            let shift = self.fetchedResultsController.fetchedObjects?[indexPath.row] as? Shift
                
            cell.configureCellWithShift(shift: shift)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let shift = self.fetchedResultsController.fetchedObjects?[indexPath.row] as? Shift
        self.selectedShift = shift
        self.performSegue(withIdentifier: kShiftFeebackSegue, sender: nil)

    }
    
    //MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ShiftFeedbackViewController {
            viewController.shift = self.selectedShift
        }
    }

    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66.0
    }

    
    //MARK: - Fetched result controller
    func initializeFetchedResultsController() {
       // let request: NSFetchRequest<NSFetchRequestResult> = Shift.fetchRequest()
        let request                 = NSFetchRequest<NSFetchRequestResult>(entityName: "Shift")
        
        let dateSort                = NSSortDescriptor(key: "date", ascending: true)

        request.sortDescriptors     = [dateSort]

        let moc = DatabaseManager.sharedInstance.persistentContainer.viewContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    

}
